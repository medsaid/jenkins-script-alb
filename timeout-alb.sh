#!/bin/bash

#list of alb arnsé
preprod="arn:aws:elasticloadbalancing:eu-west-3:547610179548:loadbalancer/app/LB-preprod/b2f2a286a8f94a3f*$1"
prod="arn:aws:elasticloadbalancing:eu-west-3:547610179548:loadbalancer/app/LB-Prod/b6bcb5969b7f97f9*$2"
qa="arn:aws:elasticloadbalancing:eu-west-3:547610179548:loadbalancer/app/LB-QA/912b57ee3a4b80c7*$3"
prodwip="arn:aws:elasticloadbalancing:eu-west-3:547610179548:loadbalancer/app/LB-Prod-WIP/b31941ee5990c44f*$4"
# albs=($preprod $prod $qa $prodwip)
albs=($preprod $prod $qa $prodwip)

echo $preprod
echo $prod
echo $qa
echo $prodwip
# #list of platform names
# names=(PREP-ROD PROD QA PROD-WIP)

# #usable function  to change alb timeout
# change_alb_timeout(){
#     output=$(aws elbv2 modify-load-balancer-attributes --load-balancer-arn $1 --attributes Key=idle_timeout.timeout_seconds,Value=$2)
# }

# #usable function to get current alb timeout
# get_current_alb_timeout_min(){
# response=$(aws elbv2 describe-load-balancer-attributes  --load-balancer-arn $1 )
# value=`echo  $response | jq '.Attributes' | jq '.[] | select(.Key=="idle_timeout.timeout_seconds") | .Value' `
# varsec=`sed -e 's/^"//' -e 's/"$//' <<<"$value"`
# echo $varsec
# }

# echo_current_timeout(){
# echo "          ----------------------------------------------------------------------------------------------------------"
# echo "          -------------------------------- CURRENT ALB TIMEOUT  : $1 MINUTES ---------------------------------------"
# echo "          ----------------------------------------------------------------------------------------------------------"
# }

# echo_changing_timeout_to(){
#     echo "          ----------------------------------------------------------------------------------------------------------"
#     echo "          -------------------------------- CHANGING ALB TIMEOUT TO $1 MINUTES --------------------------------------"
#     echo "          ----------------------------------------------------------------------------------------------------------" 
# }

# echo_changed_to() {
#     echo "          ----------------------------------------------------------------------------------------------------------"
#     echo "          ------------------------------------- TIMEOUT CHANGED TO $1 MINUTES --------------------------------------"
#     echo "          ----------------------------------------------------------------------------------------------------------"
# }


# echo "          ----------------------------------------------------------------------------------------------------------"
# echo "          ------------------------ DATE IS $(date) -------------------------------"
# echo "          ----------------------------------------------------------------------------------------------------------"

# printf "\n\n\n"

# theChanging=

# for i in "${!albs[@]}"; do
#     my_arr=($(echo ${arns[$i]} | tr "*" "\n"))
#     arn=${my_arr[0]}
#     timeout=${my_arr[1]}
#     timeout_min=$(( timeout * 60))
#     current_sec=$(get_current_alb_timeout_sec "$arn")
#     current_min=$(( varsec / 60 ))
    
#     echo "               _____________________________________________${names[$i]}______________________________________________"
#     printf "\n"
#     echo_current_timeout "$current_min"
#     echo_changing_timeout_to "$timeout_min"
#     change_alb_timeout "$arn" "$timeout"
#     echo_changed_to  "$timeout_min"
# done
